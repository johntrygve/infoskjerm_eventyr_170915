import subprocess
import shlex
import time
import os
from datetime import datetime as dt


command_line = "ping -c 1 heroku.com"
args = shlex.split(command_line)

def status():
	try:
        	subprocess.check_call(args,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        	return 1;
	except subprocess.CalledProcessError:
        	return 0;

print ""
print ""
print ""
print "-----------------------------Sjekker nettstatus----------------------------"
print "Ser om heroku.com er oppe..."
os.system("pcmanfm --set-wallpaper /home/pi/infoskjerm_eventyr_170915/avitell_logo.png")
time.sleep(10)
print ""

if status():
	print "Nett ok!"
	print "Sender restart epost..."
	os.system("python /home/pi/infoskjerm_eventyr_170915/restart_epost.py")
	time.sleep(1)	
	print "Starter app..."
	print "------------------------------------------"
	print ""
	os.system("chromium-browser --incognito --kiosk http://eventyr170224.herokuapp.com")
	exit()
else:
	print "Nett ikke tilgjengelig."
	while 3 <=dt.now().hour <=6:
		if status():
			print ""
			print "Nett ok!"
			print "Sender restart epost..."
			os.system("python /home/pi/infoskjerm_eventyr_170915/restart_epost.py")
			time.sleep(1)
			print "Starter app..."
			print "------------------------------------------"
			os.system("chromium-browser --incognito --kiosk http://eventyr170224.herokuapp.com")
			time.sleep(5)
			exit()
		else:
			os.system("pcmanfm --set-wallpaper /home/pi/infoskjerm_eventyr_170915/informasjon1.png")
			print "Nett ikke tilgjengelig."
			print "Venter 1 minutt..."
			time.sleep(60)
	else:
		test = dt.now().minute
		print "Klokka er over 6. Shutter ned om 5 minutt hvis ikke nett blir tilgjengelig..."
		os.system("pcmanfm --set-wallpaper /home/pi/infoskjerm_eventyr_170915/informasjon2.png")
		while dt.now().minute < test + 5:
			time.sleep(10)
			if status():
				print ""
				print "Nett ok!"
				print "Starter app..."
				print "Sender restart epost..."
				os.system("python /home/pi/infoskjerm_eventyr_170915/restart_epost.py")
				time.sleep(1)
				print "------------------------------------------"
				os.system("chromium-browser --incognito --kiosk http://eventyr170224.herokuapp.com")
				time.sleep(5)
				exit()
		os.system("echo Shutdown > /home/pi/pinglogg.txt")
		os.system("sudo halt")
