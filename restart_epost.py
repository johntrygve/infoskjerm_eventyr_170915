import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from datetime import datetime as dt

klokken = "{:%H:%M}".format(dt.now())
dato = "{:%d/%m/%Y}".format(dt.now())

fromaddr = "eos@avitell2.no"
toaddr = "eos@avitell2.no"
msg = MIMEMultipart()
msg['From'] = fromaddr
msg['To'] = toaddr
msg['Subject'] = "Eventyrgården infoskjerm 170915 " + klokken + " " + dato

body = "Eventyrgården infoskjerm 170915 restartet klokken: " + klokken + " den " + dato
msg.attach(MIMEText(body, 'plain'))

server = smtplib.SMTP('send.one.com', 587)
server.starttls()
server.login(fromaddr, "eos2016")
text = msg.as_string()
server.sendmail(fromaddr, toaddr, text)
server.quit()
